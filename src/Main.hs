module Main where

import Control.Concurrent (forkIO)
import Control.Concurrent.STM.TChan (TChan, newTChanIO, writeTChan, tryReadTChan)
import Control.Concurrent.STM.TVar (TVar, newTVarIO, readTVar, writeTVar)
import Control.Exception (try, SomeException, bracket)
import Control.Monad (when, unless)
import Control.Monad.Fix (fix)
import Control.Monad.STM (atomically)
import Data.Bits
import Data.Functor (void)
import Foreign hiding (void)
import Foreign.C
import System.IO (hSetBuffering, stdout, BufferMode(..))
import System.Win32 hiding (try)
import Text.Read (readEither)

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering -- Allow to ask user for input on the same line
  memName <- prompt "Select shared memory name"
  connectToMemoryMapping memName $ \memory -> do
    eventName <- prompt "Select event name"
    connectToEvent eventName $ \event -> do
      selfFire <- newTVarIO False
      printChan <- newTChanIO
      startBackgroundThread memory event printChan selfFire
      processActions memory event printChan selfFire

-- | Ask user to input string
prompt :: String -> IO String
prompt title = do
  putStr $ title ++ ": "
  getLine

-- | Prompt user any value that has 'Read' instance. Repeat if cannot parse user
-- input.
promptRead :: Read a => String -> IO a
promptRead title = fix $ \next -> do
  putStr $ title ++ ": "
  s <- getLine
  case readEither s of
    Left er -> do
      putStrLn $ "Failed to parse answer: " ++ er
      next
    Right a -> pure a

-- | Fixed size of shared memory that we allocate
memorySize :: Num a => a
memorySize = 64 * 1024 -- kilobytes

-- | Shared memory type. It is simple void*
type Memory = Ptr ()

-- | Reference to system object for event
type Event = HANDLE

-- | Wraps a creation or connection to the existing memory mapped file. Do not
-- return input handle from lambda as the handle is released immediately after
-- the function call.
connectToMemoryMapping :: String -> (Memory -> IO a) -> IO a
connectToMemoryMapping name action = bracket allocate destroy (action . snd)
  where
    allocate = do
      putStr  "Connecting to memory mapping... "
      mres <- try $ openFileMapping fILE_MAP_ALL_ACCESS False (Just name)
      file <- case mres of
        Left (e :: SomeException) -> do
          putStrLn "failed"
          putStrLn "Creating new memory mapping"
          createFileMapping Nothing pAGE_READWRITE memorySize (Just name)
        Right h -> do
          putStrLn "done"
          pure h
      memory <- mapViewOfFile file fILE_MAP_ALL_ACCESS 0 memorySize
      pure (file, memory)

    destroy (file, memory) = do
      putStrLn "Cleaning memory mapping"
      unmapViewOfFile memory
      closeHandle file

-- | Wraps openening of existing or creation of system event that tracks memory
-- mapped file modifications. Do not return input handle from labmda as the
-- handle is release immediately after the function call.
connectToEvent :: String -> (Event -> IO a) -> IO a
connectToEvent name action = bracket allocate destroy action
  where
    allocate = do
      putStr "Openning event... "
      mres <- try $ openEvent (sYNCHRONIZE .|. eVENT_MODIFY_STATE) False name
      case mres of
        Left (e :: SomeException) -> do
          putStrLn "failed"
          putStrLn "Creating new event"
          createEvent Nothing True False name
        Right h -> do
          putStrLn "done"
          pure h

    destroy event = do
      putStrLn "Cleaning event"
      closeHandle event

-- | User action to perform
data Action =
    ActionPrint -- ^ Print all messages from log
  | ActionPut -- ^ Put new message in memory mapping
  | ActionRead -- ^ Read message from memory mapping
  | ActionWait -- ^ Wait on event that signals of memory mapping modification
  | ActionExit -- ^ Exit the program

-- | Convert integral code of action to action
intToAction :: Int -> Maybe Action
intToAction i = case i of
  0 -> Just ActionPrint
  1 -> Just ActionPut
  2 -> Just ActionRead
  3 -> Just ActionWait
  4 -> Just ActionExit
  _ -> Nothing

-- | Ask user for actions
promptAction :: IO Action
promptAction = fix $ \next -> do
  i :: Int <- promptRead "Enter \t0 for printing log\n\t1 for writing message, \n\t2 for reading message, \n\t3 for waiting on event, \n\t4 for exit"
  case intToAction i of
    Nothing -> do
      putStrLn "Failed to decode action code, try again..."
      next
    Just a -> pure a

-- | Loop that asks for user input and performs one of the 'Action'
processActions :: Memory -> Event -> TChan String -> TVar Bool -> IO ()
processActions memory event printChan selfFire = fix $ \next -> do
  checkBackgroundPrints memory printChan
  a <- promptAction
  case a of
    ActionPrint -> next
    ActionPut -> do
      msg <- prompt "Message to write down"
      writeMessage memory event selfFire msg
      next
    ActionRead -> do
      putStrLn "Reading message from memory"
      putStrLn =<< readMessage memory
      next
    ActionWait -> do
      atomically $ writeTVar selfFire True -- Prevent background thread from triggering as we start listening in foreground
      putStrLn "Waiting when event fires"
      waitEventForever Nothing Nothing event
      putStrLn =<< readMessage memory
      next
    ActionExit -> putStrLn "Have a nice day!"

-- | Create background event that listens to the given event
startBackgroundThread :: Memory -> Event -> TChan String -> TVar Bool -> IO ()
startBackgroundThread memory event printChan selfFire = void . forkIO $ fix $ \next -> do
  waitEventForever (Just printChan) (Just selfFire) event
  msg <- readMessage memory
  atomically $ writeTChan printChan msg
  next

-- | Atomically check flag and reset it. Return old value.
checkSelfFire :: TVar Bool -> IO Bool
checkSelfFire tvar = atomically $ do
  res <- readTVar tvar
  when res $ writeTVar tvar False
  pure res

-- | Write string to the memory mapped file
writeMessage :: Memory -> Event -> TVar Bool -> String -> IO ()
writeMessage memory event selfFire str = withCStringLen str $ \(cstr, len) -> do
  copyBytes memory (castPtr cstr) len
  pokeByteOff memory len (0 :: CUChar)
  atomically $ writeTVar selfFire True
  pulseEvent event

-- | Read string from memory mapped file
readMessage :: Memory -> IO String
readMessage = peekCString . castPtr

-- | Hang on event until it fires. Logging channel is required to suppress printing
-- for background thread.
waitEventForever :: Maybe (TChan String) -> Maybe (TVar Bool) -> Event -> IO ()
waitEventForever mlogChan mselfFire event = fix $ \next -> do
  r <- waitForSingleObjectEx event 1000 True -- We wait with timeout as we need react on runtime exit request if user hit Ctrl+C
  if | r == wAIT_FAILED -> do
        err <- getError
        logging $ "Failed to wait on event! " ++ err
     | r == wAIT_OBJECT_0 -> do
        isSelf <- getSelf
        if isSelf then next else logging "Event fired!"
     | r == wAIT_IO_COMPLETION -> do
        logging "Async IO completion requested!"
        next
     | r == wAIT_TIMEOUT -> next
     | otherwise -> do
        logging $ "Unknown return code: " ++ show r
        next
  where
    getSelf = maybe (pure False) checkSelfFire mselfFire
    logging msg = case mlogChan of
      Nothing -> putStrLn msg
      Just logChan -> atomically $ writeTChan logChan msg

-- | Get last error string
getError :: IO String
getError = do
  i <- getLastError
  bracket (getErrorMessage i) localFree peekTString

-- | Print all messages that are signaled from background thread
checkBackgroundPrints :: Memory -> TChan String -> IO ()
checkBackgroundPrints memory chan = fix $ \next -> do
  mres <- atomically $ tryReadTChan chan
  case mres of
    Nothing -> pure ()
    Just s -> do
      putStrLn s
      next
